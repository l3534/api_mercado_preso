const express = require('express')
const Location = require('../models/Cords')

const router = express.Router()

router.get('/', (req, res) => {
    Location.find()
        .exec()
        .then(Meta => res.status(200).send(Meta))
})

router.get('/:id', (req, res) => {
    Location.findById(req.params.id)
        .exec()
        .then(Meta => res.status(200).send(Meta))
})

router.post('/', (req, res) => {
    Location.create(req.body)
        .then(Meta => res.status(201).send(Meta))
})

router.put('/:id', (req, res) => {
    Location.findByIdAndUpdate(req.params.id, req.body)
        .then(() => res.sendStatus(204))
})

router.delete('/:id', (req, res) => {
    Location.findByIdAndDelete(req.params.id)
        .exec()
        .then(() => res.sendStatus(204))
})

module.exports = router