const express = require("express");
const Products = require("../models/Products");

const router = express.Router();

router.get("/", (req, res) => {
  Products.find()
    .populate("comments")
    .exec()
    .then((Meta) => res.status(200).send(Meta));
});

router.get("/:id", (req, res) => {
  Products.findById(req.params.id)
    .exec()
    .then((Meta) => res.status(200).send(Meta));
});

router.post("/", async (req, res) => {
  Products.create(req.body).then((meta) => res.status(201).send(meta));
});

router.put("/:id", (req, res) => {
  Products.findByIdAndUpdate(req.params.id, req.body).then(() =>
    res.sendStatus(204)
  );
});

router.delete("/:id", (req, res) => {
  Products.findByIdAndDelete(req.params.id)
    .exec()
    .then(() => res.sendStatus(204));
});

router.put("/assignComment/:id", (req, res) => {
  const { comment } = req.body;
  Products.findByIdAndUpdate(req.params.id, {
    $push: { comments: comment },
  }).then(() => res.sendStatus(204));
});

module.exports = router;
