const express = require("express");
const Comments = require("../models/Comment");

const router = express.Router();

router.post("/", (req, res) => {
  Comments.create(req.body).then((meta) => res.status(201).send(meta));
});

router.put("/:id", (req, res) => {
  Comments.findByIdAndUpdate(req.params.id, req.body).then(() =>
    res.sendStatus(204)
  );
});

router.delete("/:id", (req, res) => {
  Comments.findByIdAndRemove(req.params.id)
    .exec()
    .then(() => res.sendStatus(204));
});

module.exports = router;
