const express = require("express");
const Users = require("../models/Users");
const crypto = require("crypto");
const jwt = require("jsonwebtoken");

const router = express.Router();

const signToken = (id) => {
  return jwt.sign({ id }, "meta", {
    expiresIn: 60 * 60 * 24 * 365,
  });
};

router.post("/register", (req, res) => {
  const { email, password } = req.body;

  crypto.randomBytes(16, (err, salt) => {
    const newSalt = salt.toString("base64");
    crypto.pbkdf2(password, newSalt, 10000, 64, "sha1", (err, key) => {
      const encryptedPassword = key.toString("base64");

      Users.findOne({ email })
        .exec()
        .then((meta) => {
          if (meta) {
            return res.status(400).send("El usuario ya existe");
          }
          Users.create({
            email,
            password: encryptedPassword,
            salt: newSalt,
          });
          res.status(201).send("Usuario creado con éxito");
        });
    });
  });
});

router.post("/login", (req, res) => {
  const { email, password } = req.body;

  Users.findOne({ email })
    .exec()
    .then((meta) => {
      if (!meta) {
        return res.send("Usuario y/o contraseña incorrectos");
      }
      crypto.pbkdf2(password, meta.salt, 10000, 64, "sha1", (err, key) => {
        const encryptedPassword = key.toString("base64");

        if (meta.password === encryptedPassword) {
          const token = signToken(meta.id);
          return res.send({ token });
        }
        res.send("Usuario y/o contraseña incorrectos");
      });
    });
});

module.exports = router;
