const express = require("express");
const Shops = require("../models/Shops");

const router = express.Router();

router.get("/", (req, res) => {
  Shops.find()
    .populate("products")
    .populate("coords")
    .exec()
    .then((Meta) => res.status(200).send(Meta));
});

router.get("/:id", (req, res) => {
  Shops.findById(req.params.id)
    .populate("products")
    .populate("coords")
    .exec()
    .then((Meta) => res.status(200).send(Meta));
});

router.post("/", (req, res) => {
  Shops.create(req.body).then((Meta) => res.status(201).send(Meta));
});

router.put("/:id", (req, res) => {
  Shops.findByIdAndUpdate(req.params.id, req.body).then(() =>
    res.sendStatus(204)
  );
});

router.put("/assignProduct/:id", (req, res) => {
  const { product } = req.body;
  Shops.findByIdAndUpdate(req.params.id, { $push: { products: product } }).then(
    () => res.sendStatus(204)
  );
});

router.put("/assignCoord/:id", (req, res) => {
  const { coord } = req.body;
  Shops.findByIdAndUpdate(req.params.id, { $push: { coords: coord } }).then(
    () => res.sendStatus(204)
  );
});

router.delete("/:id", (req, res) => {
  Shops.findByIdAndDelete(req.params.id)
    .exec()
    .then(() => res.sendStatus(204));
});

module.exports = router;
