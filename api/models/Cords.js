const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Cords = mongoose.model('Cord', new Schema({
    latitude: String,
    longitude: String,
}))

module.exports = Cords