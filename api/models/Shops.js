const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Shops = mongoose.model(
  "Shop",
  new Schema({
    name: String,
    imageUrl: String,
    desc: String,
    products: [{ type: Schema.Types.ObjectId, ref: "Product" }],
    coords: [{ type: Schema.Types.ObjectId, ref: "Cord" }],
  })
);

module.exports = Shops;
