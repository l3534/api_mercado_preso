const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const Products = mongoose.model(
  "Product",
  new Schema({
    name: String,
    imageUrl: String,
    desc: String,
    comments: [{ type: Schema.Types.ObjectId, ref: "Comment" }],
  })
);

module.exports = Products;
