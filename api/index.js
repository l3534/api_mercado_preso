const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoose = require("mongoose");

const auth = require("./routes/auth");
const locations = require("./routes/locations");
const products = require("./routes/products");
const shops = require("./routes/shops");
const comments = require("./routes/comments");

const app = express();
app.use(bodyParser.json());
app.use(cors());

mongoose.connect(process.env.MONGO_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

app.use("/api/auth", auth);
app.use("/api/locations", locations);
app.use("/api/products", products);
app.use("/api/shops", shops);
app.use("/api/comments", comments)

module.exports = app;
